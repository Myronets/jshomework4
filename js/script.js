// Задание
// Реализовать функцию для создания объекта "пользователь".
//
//     Технические требования:
//
//     Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
//     При вызове функция должна спросить у вызывающего имя и фамилию.
//     Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
//     Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.
//
//
//     Не обязательное задание продвинутой сложности:
//
//     Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.


createNewUser = () => {
    newUser = {};
    newUser.firstName = prompt('Input yor name','');
    newUser.lastName = prompt('Input yor last name','');
    newUser.getLogin = function() {
        let login;
        login = this.firstName.toLowerCase().charAt(0) + this.lastName.toLowerCase();
        return login;
    };
    return newUser;
};

createNewUser();
console.log(newUser);
console.log(newUser.getLogin());


Object.defineProperty(newUser, 'firstName', {
    writable: false
});

Object.defineProperty(newUser,'lastName', {
    writable: false
});

